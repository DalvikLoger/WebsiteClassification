import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as img
from matplotlib.image import imread
import seaborn as sns
sns.set_theme() 

import re
df = pd.read_csv('Web_classification.csv',header=None, error_bad_lines=False, sep=',')

df = df.rename(columns = {1 : 'id'})
df = df.rename(columns = {0 : 'size',2 : 'domain', 3 : 'category', 4 : 'title', 5 : 'description', 6 : 'Keywords'})
df['title'] = df['title'].fillna('')
df['description'] = df['description'].fillna('')
df['Keywords'] = df['Keywords'].fillna('')

from sklearn.preprocessing import LabelEncoder

# Créez une instance de LabelEncoder
label_encoder = LabelEncoder()
df['text_combined'] = df['title'] + ' ' + df['description']+ ' ' + df['domain']+ ' ' + df['Keywords']
df['category_encoded'] = label_encoder.fit_transform(df['category'])

from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(df['text_combined'], df.category_encoded, test_size=0.2, random_state=1234)

import tensorflow as tf
# Définition du tokenizer
tokenizer = tf.keras.preprocessing.text.Tokenizer(num_words=10000)
# Mettre à jour le dictionnaire du tokenizer
tokenizer.fit_on_texts(X_train)

X_train = tokenizer.texts_to_sequences(X_train)

X_test = tokenizer.texts_to_sequences(X_test)

maxlen = 500
X_train = tf.keras.preprocessing.sequence.pad_sequences(X_train, maxlen=maxlen, padding='post', truncating='post')
X_test = tf.keras.preprocessing.sequence.pad_sequences(X_test, maxlen=maxlen, padding='post', truncating='post')

from tensorflow.keras import Sequential
from tensorflow.keras.layers import Embedding, Dense, GlobalAveragePooling1D, RNN, GRUCell, Dropout
from tensorflow.keras.regularizers import l2
embedding_dim = 200

model = Sequential()
model.add(Embedding(10000, embedding_dim))
model.add(RNN(GRUCell(128), return_sequences=True))
model.add(Dropout(0.5))
model.add(GlobalAveragePooling1D())
model.add(Dense(256, activation='relu', kernel_regularizer=l2(0.01)))
model.add(Dropout(0.5))
model.add(Dense(595, activation='softmax'))

model.summary()

from tensorflow.keras.callbacks import LearningRateScheduler

def lr_schedule(epoch):
    return 0.001 * (0.1 ** int(epoch / 10))

model.compile(optimizer='adam',loss='sparse_categorical_crossentropy', metrics=['accuracy'])
lr_scheduler = LearningRateScheduler(lr_schedule)

history = model.fit(X_train, y_train.values,
    batch_size = 64,
    epochs=30,
    callbacks=[lr_scheduler],
    validation_data = [X_test, y_test.values])

plt.plot(history.history['loss'], label='train_loss')
plt.plot(history.history['val_loss'], label='val_loss')
plt.legend()
plt.show()

plt.plot(history.history['accuracy'], label='train_accuracy')
plt.plot(history.history['val_accuracy'], label='val_accuracy')
plt.legend()
plt.show()

from sklearn.metrics import accuracy_score, classification_report
y_pred = model.predict(X_test)

y_test_class = y_test
y_pred_class = np.argmax(y_pred,axis=1)
from sklearn.metrics import classification_report,confusion_matrix
print(classification_report(y_test_class,y_pred_class))
print(confusion_matrix(y_test_class,y_pred_class))
