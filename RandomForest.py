import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as img
from matplotlib.image import imread
import seaborn as sns
sns.set_theme() 

import re
df = pd.read_csv('Web_classification.csv',header=None, error_bad_lines=False, sep=',')

df = df.rename(columns = {1 : 'id'})
df = df.rename(columns = {0 : 'size',2 : 'domain', 3 : 'category', 4 : 'title', 5 : 'description', 6 : 'Keywords'})
df['title'] = df['title'].fillna('')
df['description'] = df['description'].fillna('')
df['Keywords'] = df['Keywords'].fillna('')

from sklearn.preprocessing import LabelEncoder

# Créez une instance de LabelEncoder
label_encoder = LabelEncoder()

from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score, classification_report

# Suppose df is your DataFrame
# Preprocessing (you may need to customize this based on your data)
df['text_combined'] = df['title'] + ' ' + df['description']+ ' ' + df['domain']+ ' ' + df['Keywords']

# Train-test split
train_data, test_data, train_labels, test_labels = train_test_split(df['text_combined'], df['category'], test_size=0.2, random_state=42)

# TF-IDF Vectorization
tfidf_vectorizer = TfidfVectorizer(stop_words='english', max_features=5000)
train_tfidf = tfidf_vectorizer.fit_transform(train_data)
test_tfidf = tfidf_vectorizer.transform(test_data)

# Random Forest Classifier
rf_classifier = RandomForestClassifier(n_estimators=100, random_state=42)
rf_classifier.fit(train_tfidf, train_labels)

# Predictions
predictions = rf_classifier.predict(test_tfidf)

# Evaluation
accuracy = accuracy_score(test_labels, predictions)
print(f'Accuracy: {accuracy}')
print(classification_report(test_labels, predictions))
