import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as img
from matplotlib.image import imread
import seaborn as sns
sns.set_theme() 

import re
df = pd.read_csv('Web_classification.csv',header=None, error_bad_lines=False, sep=',')

df = df.rename(columns = {1 : 'id'})
df = df.rename(columns = {0 : 'size',2 : 'domain', 3 : 'category', 4 : 'title', 5 : 'description', 6 : 'Keywords'})
df['title'] = df['title'].fillna('')
df['description'] = df['description'].fillna('')
df['Keywords'] = df['Keywords'].fillna('')

import re
import unicodedata
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
import swifter

stop_words = stopwords.words('english')

# Converts the unicode file to ascii
def unicode_to_ascii(s):
    return ''.join(c for c in unicodedata.normalize('NFD', s)
        if unicodedata.category(c) != 'Mn')

def preprocess_sentence(w):
    w = unicode_to_ascii(w.lower().strip())
    # creating a space between a word and the punctuation following it
    # eg: "he is a boy." => "he is a boy ."
    w = re.sub(r"([?.!,¿])", r" \1 ", w)
    w = re.sub(r'[" "]+', " ", w)
    # replacing everything with space except (a-z, A-Z, ".", "?", "!", ",")
    w = re.sub(r"[^a-zA-Z?.!]+", " ", w)
    w = re.sub(r'\b\w{0,2}\b', '', w)

    # remove stopword
    mots = word_tokenize(w.strip())
    mots = [mot for mot in mots if mot not in stop_words]
    return ' '.join(mots).strip()

df.title = df.title.swifter.apply(lambda x :preprocess_sentence(x))

from sklearn.preprocessing import LabelEncoder

# Créez une instance de LabelEncoder
label_encoder = LabelEncoder()

from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score, classification_report

df['text_combined'] = df['title'] + ' ' + df['description']+ ' ' + df['domain']+ ' ' + df['Keywords']
df['category_encoded'] = label_encoder.fit_transform(df['category'])
from transformers import GPT2Tokenizer
import numpy as np

# Créer un tokenizer GPT-2
tokenizer = GPT2Tokenizer.from_pretrained('gpt2')

# Ajouter un token de padding
tokenizer.pad_token = tokenizer.eos_token

# Fonction de prétraitement pour tokeniser le texte et convertir en NumPy
def preprocess_text(text):
    encoded_text = tokenizer(text, return_tensors="tf", truncation=True, padding=True, max_length=50)  # Ajuster la longueur maximale
    return {
        'input_ids': np.array(encoded_text['input_ids']),
        'attention_mask': np.array(encoded_text['attention_mask'])
    }

# Appliquer la tokenisation à la colonne 'text_combined'
df['tokenized_text'] = df['text_combined'].apply(preprocess_text)

# Assurez-vous que toutes les séquences ont la même longueur
max_length = max(df['tokenized_text'].apply(lambda x: x['input_ids'].shape[1]))
df['tokenized_text'] = df['tokenized_text'].apply(lambda x: {'input_ids': np.pad(x['input_ids'], (0, max_length - x['input_ids'].shape[1])), 'attention_mask': np.pad(x['attention_mask'], (0, max_length - x['attention_mask'].shape[1]))})

# Concaténer les listes pour obtenir des tableaux NumPy
label = np.concatenate(df['tokenized_text'].apply(lambda x: x['input_ids']))
attention_mask_train = np.concatenate(df['tokenized_text'].apply(lambda x: x['attention_mask']))
target = df['category_encoded'].values

from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(label, target, test_size=0.2, random_state=1234)

import tensorflow as tf
from transformers import GPT2Tokenizer, TFGPT2ForSequenceClassification
from tensorflow.keras import Input, Model
from tensorflow.keras.layers import Dense, Dropout
from tensorflow.keras.utils import to_categorical

# Convertir les étiquettes en one-hot encoding
one_hot_labels = to_categorical(y_train, num_classes=595)

# Charger le tokenizer GPT-2
tokenizer = GPT2Tokenizer.from_pretrained('gpt2')
max_len = 500

# Entrée du modèle
input_layer = Input(shape=(max_len,), dtype=tf.int32)

# Couche GPT-2
gpt2_output = TFGPT2ForSequenceClassification.from_pretrained('gpt2', num_labels=595, trainable=False)(input_layer)
logits = gpt2_output.logits  # Récupérer les logits à partir de l'objet de sortie

# Couches de votre modèle
x = Dropout(0.5)(logits)
x = Dense(256, activation='relu')(x)
x = Dropout(0.5)(x)
output_layer = Dense(595, activation='softmax')(x)

# Créer le modèle
model = Model(inputs=input_layer, outputs=output_layer)

model.summary()

from tensorflow.keras.optimizers import Adam
from tensorflow.keras.callbacks import LearningRateScheduler

def lr_schedule(epoch):
    return 0.001 * (0.1 ** int(epoch / 10))

# Utiliser categorical_crossentropy comme fonction de perte
model.compile(optimizer=Adam(), loss='categorical_crossentropy', metrics=['accuracy'])
lr_scheduler = LearningRateScheduler(lr_schedule)

# Entraîner le modèle
history = model.fit(X_train, y_train,
                    batch_size=64,
                    epochs=3,
                    callbacks=[lr_scheduler],
                    validation_data=(X_test, to_categorical(y_test, num_classes=595)))

# Évaluer le modèle avec la longueur correcte des étiquettes de test
validation_predictions = model.predict(X_test)
truncated_y_test = to_categorical(y_test, num_classes=595)[:, :validation_predictions.shape[1], :]

evaluation = model.evaluate(X_test, truncated_y_test)
print("Loss:", evaluation[0])
print("Accuracy:", evaluation[1])

